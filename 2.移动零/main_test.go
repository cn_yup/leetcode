package main

import "testing"

//  SliceEq
//  @Description:  	判断两个int类型的切片是否相等
//  @param a		切片
//  @param b		切片
//  @return bool	相等返回ture 否则返回false
func SliceEq(a, b []int) bool {
	if (a == nil) != (b == nil) {
		return false
	}

	if len(a) != len(b) {
		return false
	}

	for i := range a {
		if a[i] != b[i] {
			return false
		}
	}
	return true
}

func TestMoveZeroes(t *testing.T) {

	cases := []struct {
		List     []int
		Expected []int
	}{
		{[]int{0, 1, 0, 3, 12}, []int{1, 3, 12, 0, 0}},
		{[]int{3, 2, 4, 0, 1}, []int{3, 2, 4, 1, 0}},
		{[]int{0}, []int{0}},
	}

	for _, c := range cases {
		t.Run("violence", func(t *testing.T) {
			if ans := MoveZeroes(c.List); !SliceEq(ans, c.Expected) {
				t.Fatal("expected list is ", c.Expected, " but got list ", ans)
			}
		})

	}
}
