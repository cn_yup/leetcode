package main

// MoveZeroes
//  @Description:	移动零值 设定一个临时变量 k = 0，遍历数组 nums，将非零元素与之前的零元素进行交换，维护变量k的值。
//  @param list		需要处理的列表
//  @return []int	处理完成的列表
func MoveZeroes(list []int) []int {
	j := 0

	for i := 0; i < len(list); i++ {
		if list[i] != 0 {
			if i != j {
				list[i], list[j] = list[j], list[i]
			}
			j++
		}
	}
	return list
}

func main() {

}
