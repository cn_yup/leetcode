package main

//  containsDuplicateHash
//  @Description:  		使用map方式解决
//  @param nums			待处理的列表
//  @return bool		存在重复元素返回true，否则返回false
func containsDuplicateHash(nums []int) bool {
	tmpMap := map[int]int{}

	for i := 0; i < len(nums); i++ {
		if _, ok := tmpMap[i]; !ok {
			return true
		} else {
			tmpMap[i] = nums[i]
		}
	}
	return false
}

func main() {

}
