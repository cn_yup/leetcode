package main

import "testing"

func TestContainsDuplicateHash(t *testing.T) {

	cases := []struct {
		List     []int
		Expected bool
	}{
		{[]int{1, 2, 3, 1}, true},
		{[]int{1, 2, 3, 4}, false},
		{[]int{1, 1, 1, 3, 3, 4, 3, 2, 4, 2}, true},
	}

	for _, c := range cases {
		t.Run("violence", func(t *testing.T) {
			if ans := containsDuplicateHash(c.List); ans != c.Expected {
				t.Fatal("expected ans is ", c.Expected, " but got ans ", ans)
			}
		})
	}
}
