package main

import "testing"

func TestTwoNumSum(t *testing.T) {

	cases := []struct {
		List     []int
		Target   int
		Expected []int
	}{
		{[]int{2, 7, 11, 15}, 9, []int{0, 1}},
		{[]int{3, 2, 4}, 6, []int{1, 2}},
		{[]int{3, 3}, 6, []int{0, 1}},
	}

	for _, c := range cases {
		t.Run("violence", func(t *testing.T) {
			if ans := violence(c.List, c.Target); ans[0] != c.Expected[0] && ans[1] != c.Expected[1] {
				t.Fatalf("list error, indexOne is %d, indexTwo is %d, but got is %d, %d",
					c.Expected[0], c.Expected[1], ans[0], ans[1])
			}
		})
		t.Run("mapHash", func(t *testing.T) {
			if ans := mapHash(c.List, c.Target); ans[0] != c.Expected[0] && ans[1] != c.Expected[1] {
				t.Fatalf("list error, indexOne is %d, indexTwo is %d, but got is %d, %d",
					c.Expected[0], c.Expected[1], ans[0], ans[1])
			}
		})
	}
}
