package main

//  violence
//  @Description: 	暴力方法解决两数之和问题 通过两个循环来遍历出结果
//  @param list  	原始列表
//  @param target  	取和的值
//  @return []int  	符合要求的元素的索引

func violence(list []int, target int) []int {
	for i := 0; i < len(list)-1; i++ {
		for j := i + 1; j < len(list); j++ {
			if list[i]+list[j] == target {
				return []int{i, j}
			}
		}
	}
	return []int{-1, -1}
}

//  mapHash
//  @Description:	使用map 哈希
//  @param list  	原始列表
//  @param target  	取和的值
//  @return []int  	符合要求的元素的索引

func mapHash(list []int, target int) []int {
	resMap := map[int]int{}

	for index, value := range list {
		// 判断target减去当前遍历到的value的值(val)在不在map中，如果在则直接返回遍历的value的索引和map中以val为key的对应的值
		if val, ok := resMap[target-value]; ok {
			return []int{val, index}
		} else { // 如果map中不存在，则key为值，value为索引 加入到map中
			resMap[value] = index
		}
	}
	return []int{-1, -1}
}

func main() {

}
