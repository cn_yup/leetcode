package main

//  singleNumberHash
//  @Description:  	使用hash记录元素出现的个数
//  @param nums	   	需要处理的数组
//  @return int		只出现一次的值
func singleNumberHash(nums []int) int {
	tmpMap := map[int]int{}

	for _, value := range nums {
		tmpMap[value]++
	}

	for k, v := range tmpMap {
		if v == 1 {
			return k
		}
	}
	return -1
}

//  singleNumberHashBool
//  @Description:  	使用hash表  如果元素是重复的(value 为ture)则删除
//  @param nums	   	需要处理的数组
//  @return int		只出现一次的值
func singleNumberHashBool(nums []int) int {
	tmpMap := map[int]bool{}

	for _, value := range nums {
		if tmpMap[value] {
			delete(tmpMap, value)
		} else {
			tmpMap[value] = true
		}
	}

	res := 0
	for value := range tmpMap {
		res = value
	}
	return res
}

//  singleNumberBiteXor
//  @Description:  	按位异或解决该题  A^A=0 0^A=A   A^B^A^B^C = C
//  @param nums	   	需要处理的数组
//  @return int		只出现一次的值
func singleNumberBiteXor(nums []int) int {
	res := 0
	for _, value := range nums {
		res ^= value
	}
	return res
}

func main() {

}
