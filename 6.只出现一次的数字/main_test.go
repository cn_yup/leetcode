package main

import "testing"

func TestSingleNumberHash(t *testing.T) {
	cases := []struct {
		List     []int
		Expected int
	}{
		{[]int{2, 2, 1}, 1},
		{[]int{4, 1, 2, 1, 2}, 4},
	}

	for _, c := range cases {
		t.Helper()
		t.Run("singleNumberHash", func(t *testing.T) {
			if ans := singleNumberHash(c.List); ans != c.Expected {
				t.Fatalf("%d i need to get, but i get %d, it is not Expected", c.Expected, ans)
			}
		})
		t.Run("singleNumberHashBool", func(t *testing.T) {
			if ans := singleNumberHashBool(c.List); ans != c.Expected {
				t.Fatalf("%d i need to get, but i get %d, it is not Expected", c.Expected, ans)
			}
		})
		t.Run("singleNumberBiteXor", func(t *testing.T) {
			if ans := singleNumberBiteXor(c.List); ans != c.Expected {
				t.Fatalf("%d i need to get, but i get %d, it is not Expected", c.Expected, ans)
			}
		})
	}
}
