
####给你一个整数数组nums和一个整数k ，
####判断数组中是否存在两个不同的索引i和j ，满足 nums[i] == nums[j] 且 abs(i - j) <= k 。
####如果存在，返回 true ；否则，返回 false 。


*示例1*
```
输入：nums = [1,2,3,1], k = 3
输出：true
```

*示例2*
```
输入：nums = [1,0,1,1], k = 1
输出：true
```

*示例3*
```
输入：nums = [1,2,3,1,2,3], k = 2
输出：false
```

*提示:*
```
1 <= nums.length <= 10^5
-10^9 <= nums[i] <= 10^9
```
