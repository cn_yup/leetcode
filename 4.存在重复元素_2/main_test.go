package main

import "testing"

func TestContainsNearbyDuplicate(t *testing.T) {
	t.Helper()
	cases := []struct {
		List     []int
		Target   int
		Expected bool
	}{
		{[]int{1, 2, 3, 1}, 3, true},
		{[]int{1, 0, 1, 1}, 1, true},
		{[]int{1, 2, 3, 1, 2, 3}, 2, false},
	}

	for _, c := range cases {
		t.Run("violence", func(t *testing.T) {
			if ans := ContainsNearbyDuplicate(c.List, c.Target); ans != c.Expected {
				t.Fatal("expected ans is ", c.Expected, " but got ans ", ans)
			}
		})
	}
}
