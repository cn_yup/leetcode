package main

//  ContainsNearbyDuplicate
//  @Description: 		使用map方式解决
//  @param nums			待处理的列表
//  @param target		用以对比的值
//  @return bool		存在重复元素返回true，否则返回false

func ContainsNearbyDuplicate(nums []int, target int) bool {
	tmpMap := map[int]int{}

	// 以值为key index为val
	// index 的值肯定会大于 map中所存在的val的值
	for index, value := range nums {
		if val, ok := tmpMap[value]; ok && index-val <= target {
			return true
		} else {
			tmpMap[value] = index
		}
	}
	return false
}

func main() {
}
