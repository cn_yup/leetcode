package main

import "testing"

//  SliceEq
//  @Description:  	判断两个int类型的切片是否相等
//  @param a		切片
//  @param b		切片
//  @return bool	相等返回ture 否则返回false
func SliceEq(a, b []int) bool {
	if (a == nil) != (b == nil) {
		return false
	}

	if len(a) != len(b) {
		return false
	}

	for i := range a {
		if a[i] != b[i] {
			return false
		}
	}
	return true
}

func TestPlusOne(t *testing.T) {
	t.Helper()
	cases := []struct {
		List   []int
		Target []int
	}{
		{[]int{1, 2, 3}, []int{1, 2, 4}},
		{[]int{1, 0, 1, 1}, []int{1, 0, 1, 2}},
		{[]int{4, 3, 2, 1}, []int{4, 3, 2, 2}},
		{[]int{9, 9, 9}, []int{1, 0, 0, 0}},
		{[]int{0}, []int{1}},
	}

	for _, c := range cases {
		t.Run("plusOne", func(t *testing.T) {
			if ans := plusOne(c.List); !SliceEq(ans, c.Target) {
				t.Fatal("expected ans is ", c.Target, " but got ans ", ans)
			}
		})
	}
}
