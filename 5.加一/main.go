package main

// 倒序循环遍历列表 如果值< 9 则值加一返回 ，如果值==9 则置为0 然后继续循环， 如果循环结束了还没有结束，则说明第一位也是9(ex： 999)
// 则数组长度加一 返回0

//  plusOne
//  @Description:	加一
//  @param digits	需要处理的数组
//  @return []int	返回加一后的数组

func plusOne(digits []int) []int {

	for i := len(digits) - 1; i >= 0; i-- {
		if digits[i] == 9 {
			digits[i] = 0
		} else {
			digits[i]++
			return digits
		}
	}

	digits = make([]int, len(digits)+1)
	digits[0] = 1
	return digits
}

func main() {
}
